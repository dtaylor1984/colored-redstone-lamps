package com.dtaylor.crl.creativetab;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CRLCreativeTab extends CreativeTabs
{
	private Item item;
	
	public CRLCreativeTab( String label )
	{
		super( label );
	}
	
	public void setIcon( Item item )
	{
		this.item = item;
	}

	@Override
	public Item getTabIconItem()
	{
		return this.item;
	}
}
