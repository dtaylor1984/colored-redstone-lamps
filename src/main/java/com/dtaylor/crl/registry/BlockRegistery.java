package com.dtaylor.crl.registry;

import com.dtaylor.crl.reference.Reference;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;

public class BlockRegistery
{
	public static void register( Block block )
	{
		GameRegistry.registerBlock( block, Reference.MOD_ID + "_" + block.getUnlocalizedName().substring(5) );
	}
}
