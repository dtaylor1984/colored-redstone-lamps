package com.dtaylor.crl.crafting;

import com.dtaylor.crl.block.CRLBlocks;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.GameRegistry;

public class CRLCrafting
{
	public static void init()
	{
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampBlack), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 0 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampRed), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 1 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampGreen), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 2 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampBrown), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 3 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampBlue), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 4 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampPurple), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 5 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampCyan), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 6 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampLightGray), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 7 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampGray), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 8 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampPink), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 9 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampLime), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 10 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampYellow), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 11 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampLightBlue), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 12 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampMagenta), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 13 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampOrange), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 14 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
		GameRegistry.addRecipe(new ItemStack(CRLBlocks.lampWhite), new Object[] {"CDC", "GRG", "CDC", 'C', Blocks.glass_pane, 'D', new ItemStack( Items.dye, 1, 15 ), 'G', Items.glowstone_dust, 'R', Items.redstone});
	}
}
