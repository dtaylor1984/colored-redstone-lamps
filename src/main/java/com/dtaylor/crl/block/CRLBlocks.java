package com.dtaylor.crl.block;

import com.dtaylor.crl.reference.LampColor;
import com.dtaylor.crl.registry.BlockRegistery;

import net.minecraft.block.Block;

public class CRLBlocks
{
	public static Block lampRed;
	public static Block lampRedLit;

	public static Block lampOrange;
	public static Block lampOrangeLit;

	public static Block lampYellow;
	public static Block lampYellowLit;

	public static Block lampGreen;
	public static Block lampGreenLit;

	public static Block lampBlue;
	public static Block lampBlueLit;

	public static Block lampLightBlue;
	public static Block lampLightBlueLit;

	public static Block lampMagenta;
	public static Block lampMagentaLit;

	public static Block lampPink;
	public static Block lampPinkLit;

	public static Block lampWhite;
	public static Block lampWhiteLit;

	public static Block lampLightGray;
	public static Block lampLightGrayLit;

	public static Block lampBlack;
	public static Block lampBlackLit;

	public static Block lampBrown;
	public static Block lampBrownLit;

	public static Block lampCyan;
	public static Block lampCyanLit;

	public static Block lampPurple;
	public static Block lampPurpleLit;

	public static Block lampGray;
	public static Block lampGrayLit;

	public static Block lampLime;
	public static Block lampLimeLit;

	public static void init()
	{
		lampRed = new Lamp( false, LampColor.RED );
		lampRedLit = new Lamp( true, LampColor.RED );

		lampOrange = new Lamp( false, LampColor.ORANGE );
		lampOrangeLit = new Lamp( true, LampColor.ORANGE );

		lampYellow = new Lamp( false, LampColor.YELLOW );
		lampYellowLit = new Lamp( true, LampColor.YELLOW );

		lampGreen = new Lamp( false, LampColor.GREEN );
		lampGreenLit = new Lamp( true, LampColor.GREEN );

		lampBlue = new Lamp( false, LampColor.BLUE );
		lampBlueLit = new Lamp( true, LampColor.BLUE );

		lampLightBlue = new Lamp( false, LampColor.LIGHT_BLUE );
		lampLightBlueLit = new Lamp( true, LampColor.LIGHT_BLUE );

		lampMagenta = new Lamp( false, LampColor.MAGENTA );
		lampMagentaLit = new Lamp( true, LampColor.MAGENTA );

		lampPink = new Lamp( false, LampColor.PINK );
		lampPinkLit = new Lamp( true, LampColor.PINK );

		lampWhite = new Lamp( false, LampColor.WHITE );
		lampWhiteLit = new Lamp( true, LampColor.WHITE );

		lampLightGray = new Lamp( false, LampColor.LIGHT_GRAY );
		lampLightGrayLit = new Lamp( true, LampColor.LIGHT_GRAY );

		lampBlack = new Lamp( false, LampColor.BLACK );
		lampBlackLit = new Lamp( true, LampColor.BLACK );

		lampBrown = new Lamp( false, LampColor.BROWN );
		lampBrownLit = new Lamp( true, LampColor.BROWN );

		lampCyan = new Lamp( false, LampColor.CYAN );
		lampCyanLit = new Lamp( true, LampColor.CYAN );

		lampPurple = new Lamp( false, LampColor.PURPLE );
		lampPurpleLit = new Lamp( true, LampColor.PURPLE );

		lampGray = new Lamp( false, LampColor.GRAY );
		lampGrayLit = new Lamp( true, LampColor.GRAY );

		lampLime = new Lamp( false, LampColor.LIME );
		lampLimeLit = new Lamp( true, LampColor.LIME );

		BlockRegistery.register( lampRed );
		BlockRegistery.register( lampRedLit );

		BlockRegistery.register( lampOrange );
		BlockRegistery.register( lampOrangeLit );

		BlockRegistery.register( lampYellow );
		BlockRegistery.register( lampYellowLit );

		BlockRegistery.register( lampGreen );
		BlockRegistery.register( lampGreenLit );

		BlockRegistery.register( lampBlue );
		BlockRegistery.register( lampBlueLit );

		BlockRegistery.register( lampLightBlue );
		BlockRegistery.register( lampLightBlueLit );

		BlockRegistery.register( lampMagenta );
		BlockRegistery.register( lampMagentaLit );

		BlockRegistery.register( lampPink );
		BlockRegistery.register( lampPinkLit );

		BlockRegistery.register( lampWhite );
		BlockRegistery.register( lampWhiteLit );

		BlockRegistery.register( lampLightGray );
		BlockRegistery.register( lampLightGrayLit );

		BlockRegistery.register( lampBlack );
		BlockRegistery.register( lampBlackLit );

		BlockRegistery.register( lampBrown );
		BlockRegistery.register( lampBrownLit );

		BlockRegistery.register( lampCyan );
		BlockRegistery.register( lampCyanLit );

		BlockRegistery.register( lampPurple );
		BlockRegistery.register( lampPurpleLit );

		BlockRegistery.register( lampGray );
		BlockRegistery.register( lampGrayLit );

		BlockRegistery.register( lampLime );
		BlockRegistery.register( lampLimeLit );
	}
}
