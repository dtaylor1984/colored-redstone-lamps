package com.dtaylor.crl.block;

import java.util.Random;

import com.dtaylor.crl.CRL;
import com.dtaylor.crl.reference.LampColor;
import com.dtaylor.crl.reference.Reference;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.world.World;

public class Lamp extends Block
{
	private final boolean isLit;
	private final LampColor color;
	private String colorString = "";

	public Lamp( boolean isLit, LampColor color )
	{
		super( Material.redstoneLight );

		this.isLit = isLit;
		this.color = color;

		switch ( this.color )
		{
			case RED:
				this.colorString = "red";
				break;

			case BLACK:
				this.colorString = "black";
				break;

			case BLUE:
				this.colorString = "blue";
				break;

			case BROWN:
				this.colorString = "brown";
				break;

			case CYAN:
				this.colorString = "cyan";
				break;

			case GREEN:
				this.colorString = "green";
				break;

			case GRAY:
				this.colorString = "gray";
				break;

			case LIGHT_BLUE:
				this.colorString = "light_blue";
				break;

			case LIGHT_GRAY:
				this.colorString = "light_gray";
				break;

			case LIME:
				this.colorString = "lime";
				break;

			case MAGENTA:
				this.colorString = "magenta";
				break;

			case ORANGE:
				this.colorString = "orange";
				break;

			case PINK:
				this.colorString = "pink";
				break;

			case PURPLE:
				this.colorString = "purple";
				break;

			case WHITE:
				this.colorString = "white";
				break;

			case YELLOW:
				this.colorString = "yellow";
				break;

			default:
				break;
		}

		if ( this.isLit )
		{
			setLightLevel( 1.0F );
			setBlockName( "lamp_" + this.colorString + "_lit" );
		}
		else
		{
			setBlockName( "lamp_" + this.colorString );
			setCreativeTab( CRL.creativeTab );
		}

		setBlockTextureName( Reference.MOD_ID + ":" + getUnlocalizedName().substring( 5 ) );
		setHardness( 0.3F );
		setStepSound( soundTypeGlass );
	}

	public void onBlockAdded( World world, int x, int y, int z )
	{
		if ( !world.isRemote )
		{
			if ( this.isLit && !world.isBlockIndirectlyGettingPowered( x, y, z ) )
			{
				world.scheduleBlockUpdate( x, y, z, this, 4 );
			}
			else if ( !this.isLit && world.isBlockIndirectlyGettingPowered( x, y, z ) )
			{
				switch ( this.color )
				{
					case RED:
						world.setBlock( x, y, z, CRLBlocks.lampRedLit, 0, 2 );
						break;

					case BLACK:
						world.setBlock( x, y, z, CRLBlocks.lampBlackLit, 0, 2 );
						break;

					case BLUE:
						world.setBlock( x, y, z, CRLBlocks.lampBlueLit, 0, 2 );
						break;

					case BROWN:
						world.setBlock( x, y, z, CRLBlocks.lampBrownLit, 0, 2 );
						break;

					case CYAN:
						world.setBlock( x, y, z, CRLBlocks.lampCyanLit, 0, 2 );
						break;

					case GRAY:
						world.setBlock( x, y, z, CRLBlocks.lampGrayLit, 0, 2 );
						break;

					case GREEN:
						world.setBlock( x, y, z, CRLBlocks.lampGreenLit, 0, 2 );
						break;

					case LIGHT_BLUE:
						world.setBlock( x, y, z, CRLBlocks.lampLightBlueLit, 0, 2 );
						break;

					case LIGHT_GRAY:
						world.setBlock( x, y, z, CRLBlocks.lampLightGrayLit, 0, 2 );
						break;

					case LIME:
						world.setBlock( x, y, z, CRLBlocks.lampLimeLit, 0, 2 );
						break;

					case MAGENTA:
						world.setBlock( x, y, z, CRLBlocks.lampMagentaLit, 0, 2 );
						break;

					case ORANGE:
						world.setBlock( x, y, z, CRLBlocks.lampOrangeLit, 0, 2 );
						break;

					case PINK:
						world.setBlock( x, y, z, CRLBlocks.lampPink, 0, 2 );
						break;

					case PURPLE:
						world.setBlock( x, y, z, CRLBlocks.lampPurpleLit, 0, 2 );
						break;

					case WHITE:
						world.setBlock( x, y, z, CRLBlocks.lampWhiteLit, 0, 2 );
						break;

					case YELLOW:
						world.setBlock( x, y, z, CRLBlocks.lampYellowLit, 0, 2 );
						break;

					default:
						break;
				}
			}
		}
	}

	public void onNeighborBlockChange( World world, int x, int y, int z, Block block )
	{
		if ( !world.isRemote )
		{
			if ( this.isLit && !world.isBlockIndirectlyGettingPowered( x, y, z ) )
			{
				world.scheduleBlockUpdate( x, y, z, this, 4 );
			}
			else if ( !this.isLit && world.isBlockIndirectlyGettingPowered( x, y, z ) )
			{
				switch ( this.color )
				{
					case RED:
						world.setBlock( x, y, z, CRLBlocks.lampRedLit, 0, 2 );
						break;

					case BLACK:
						world.setBlock( x, y, z, CRLBlocks.lampBlackLit, 0, 2 );
						break;

					case BLUE:
						world.setBlock( x, y, z, CRLBlocks.lampBlueLit, 0, 2 );
						break;

					case BROWN:
						world.setBlock( x, y, z, CRLBlocks.lampBrownLit, 0, 2 );
						break;

					case CYAN:
						world.setBlock( x, y, z, CRLBlocks.lampCyanLit, 0, 2 );
						break;

					case GRAY:
						world.setBlock( x, y, z, CRLBlocks.lampGrayLit, 0, 2 );
						break;

					case GREEN:
						world.setBlock( x, y, z, CRLBlocks.lampGreenLit, 0, 2 );
						break;

					case LIGHT_BLUE:
						world.setBlock( x, y, z, CRLBlocks.lampLightBlueLit, 0, 2 );
						break;

					case LIGHT_GRAY:
						world.setBlock( x, y, z, CRLBlocks.lampLightGrayLit, 0, 2 );
						break;

					case LIME:
						world.setBlock( x, y, z, CRLBlocks.lampLimeLit, 0, 2 );
						break;

					case MAGENTA:
						world.setBlock( x, y, z, CRLBlocks.lampMagentaLit, 0, 2 );
						break;

					case ORANGE:
						world.setBlock( x, y, z, CRLBlocks.lampOrangeLit, 0, 2 );
						break;

					case PINK:
						world.setBlock( x, y, z, CRLBlocks.lampPink, 0, 2 );
						break;

					case PURPLE:
						world.setBlock( x, y, z, CRLBlocks.lampPurpleLit, 0, 2 );
						break;

					case WHITE:
						world.setBlock( x, y, z, CRLBlocks.lampWhiteLit, 0, 2 );
						break;

					case YELLOW:
						world.setBlock( x, y, z, CRLBlocks.lampYellowLit, 0, 2 );
						break;

					default:
						break;
				}
			}
		}
	}

	public void updateTick( World world, int x, int y, int z, Random rnd )
	{
		if ( !world.isRemote && this.isLit && !world.isBlockIndirectlyGettingPowered( x, y, z ) )
		{
			switch ( this.color )
			{
				case RED:
					world.setBlock( x, y, z, CRLBlocks.lampRed, 0, 2 );
					break;

				case BLACK:
					world.setBlock( x, y, z, CRLBlocks.lampBlack, 0, 2 );
					break;

				case BLUE:
					world.setBlock( x, y, z, CRLBlocks.lampBlue, 0, 2 );
					break;

				case BROWN:
					world.setBlock( x, y, z, CRLBlocks.lampBrown, 0, 2 );
					break;

				case CYAN:
					world.setBlock( x, y, z, CRLBlocks.lampCyan, 0, 2 );
					break;

				case GRAY:
					world.setBlock( x, y, z, CRLBlocks.lampGray, 0, 2 );
					break;

				case GREEN:
					world.setBlock( x, y, z, CRLBlocks.lampGreen, 0, 2 );
					break;

				case LIGHT_BLUE:
					world.setBlock( x, y, z, CRLBlocks.lampLightBlue, 0, 2 );
					break;

				case LIGHT_GRAY:
					world.setBlock( x, y, z, CRLBlocks.lampLightGray, 0, 2 );
					break;

				case LIME:
					world.setBlock( x, y, z, CRLBlocks.lampLime, 0, 2 );
					break;

				case MAGENTA:
					world.setBlock( x, y, z, CRLBlocks.lampMagenta, 0, 2 );
					break;

				case ORANGE:
					world.setBlock( x, y, z, CRLBlocks.lampOrange, 0, 2 );
					break;

				case PINK:
					world.setBlock( x, y, z, CRLBlocks.lampPink, 0, 2 );
					break;

				case PURPLE:
					world.setBlock( x, y, z, CRLBlocks.lampPurple, 0, 2 );
					break;

				case WHITE:
					world.setBlock( x, y, z, CRLBlocks.lampWhite, 0, 2 );
					break;

				case YELLOW:
					world.setBlock( x, y, z, CRLBlocks.lampYellow, 0, 2 );
					break;

				default:
					break;
			}
		}
	}

	public Item getItemDropped( int x, Random y, int z )
	{
		switch ( this.color )
		{
			case RED:
				return Item.getItemFromBlock( CRLBlocks.lampRed );

			case BLACK:
				return Item.getItemFromBlock( CRLBlocks.lampBlack );

			case BLUE:
				return Item.getItemFromBlock( CRLBlocks.lampBlue );

			case BROWN:
				return Item.getItemFromBlock( CRLBlocks.lampBrown );

			case CYAN:
				return Item.getItemFromBlock( CRLBlocks.lampCyan );

			case GRAY:
				return Item.getItemFromBlock( CRLBlocks.lampGray );

			case GREEN:
				return Item.getItemFromBlock( CRLBlocks.lampGreen );

			case LIGHT_BLUE:
				return Item.getItemFromBlock( CRLBlocks.lampLightBlue );

			case LIGHT_GRAY:
				return Item.getItemFromBlock( CRLBlocks.lampLightGray );

			case LIME:
				return Item.getItemFromBlock( CRLBlocks.lampLime );

			case MAGENTA:
				return Item.getItemFromBlock( CRLBlocks.lampMagenta );

			case ORANGE:
				return Item.getItemFromBlock( CRLBlocks.lampOrange );

			case PINK:
				return Item.getItemFromBlock( CRLBlocks.lampPink );

			case PURPLE:
				return Item.getItemFromBlock( CRLBlocks.lampPurple );

			case WHITE:
				return Item.getItemFromBlock( CRLBlocks.lampWhite );

			case YELLOW:
				return Item.getItemFromBlock( CRLBlocks.lampYellow );

			default:
				break;
		}

		return null;
	}

	@SideOnly( Side.CLIENT )
	public Item getItem( World world, int x, int y, int z )
	{
		switch ( this.color )
		{
			case RED:
				return Item.getItemFromBlock( CRLBlocks.lampRed );

			case BLACK:
				return Item.getItemFromBlock( CRLBlocks.lampBlack );

			case BLUE:
				return Item.getItemFromBlock( CRLBlocks.lampBlue );

			case BROWN:
				return Item.getItemFromBlock( CRLBlocks.lampBrown );

			case CYAN:
				return Item.getItemFromBlock( CRLBlocks.lampCyan );

			case GRAY:
				return Item.getItemFromBlock( CRLBlocks.lampGray );

			case GREEN:
				return Item.getItemFromBlock( CRLBlocks.lampGreen );

			case LIGHT_BLUE:
				return Item.getItemFromBlock( CRLBlocks.lampLightBlue );

			case LIGHT_GRAY:
				return Item.getItemFromBlock( CRLBlocks.lampLightGray );

			case LIME:
				return Item.getItemFromBlock( CRLBlocks.lampLime );

			case MAGENTA:
				return Item.getItemFromBlock( CRLBlocks.lampMagenta );

			case ORANGE:
				return Item.getItemFromBlock( CRLBlocks.lampOrange );

			case PINK:
				return Item.getItemFromBlock( CRLBlocks.lampPink );

			case PURPLE:
				return Item.getItemFromBlock( CRLBlocks.lampPurple );

			case WHITE:
				return Item.getItemFromBlock( CRLBlocks.lampWhite );

			case YELLOW:
				return Item.getItemFromBlock( CRLBlocks.lampYellow );

			default:
				break;
		}

		return null;
	}
}
