package com.dtaylor.crl.reference;

public enum LampColor {
	RED, ORANGE, YELLOW, GREEN, BLUE, LIGHT_BLUE, MAGENTA, PINK, WHITE, LIGHT_GRAY, BLACK, BROWN, CYAN, PURPLE, GRAY, LIME;
}
