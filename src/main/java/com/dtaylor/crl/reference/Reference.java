package com.dtaylor.crl.reference;

public class Reference
{
	public static final String MOD_NAME = "Colored Redstone Lamps";
	public static final String MOD_ID = "crl";
	public static final String VERSION = "1.0.0 Alpha";
}
