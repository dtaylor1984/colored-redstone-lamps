package com.dtaylor.crl;

import net.minecraft.item.Item;

import com.dtaylor.crl.block.CRLBlocks;
import com.dtaylor.crl.crafting.CRLCrafting;
import com.dtaylor.crl.creativetab.CRLCreativeTab;
import com.dtaylor.crl.reference.Reference;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;

@Mod( modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.VERSION )
public class CRL
{
	public static final CRLCreativeTab creativeTab = new CRLCreativeTab( "crl" );
	
	@EventHandler
	public void preInit( FMLInitializationEvent event )
	{
	}

	@EventHandler
	public void init( FMLInitializationEvent event )
	{
		CRLBlocks.init();
		CRLCrafting.init();
			
		creativeTab.setIcon( Item.getItemFromBlock(CRLBlocks.lampWhite) );
	}

	@EventHandler
	public void postInit( FMLPostInitializationEvent event )
	{

	}
}
